import java.util.Scanner;

public class Shop {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Album[] albums = new Album[4];
        for (int i = 0; i < albums.length; i++) {
            System.out.println("\nEnter the inputs for album #" + (i + 1) + " as:\ncost (double)\nyear of publication (int)\ntitle (string)\n");
            albums[i] = new Album(Double.parseDouble(sc.nextLine()), Integer.parseInt(sc.nextLine()), sc.nextLine());
        }

        //softcoded
        System.out.println("Before: ");
        System.out.println(albums[albums.length - 1]);

//        albums[albums.length - 1].setCost(Double.parseDouble(sc.nextLine())); //you told me to remove a setter, so I can't set this one
        albums[albums.length - 1].setPubYear(Integer.parseInt(sc.nextLine()));
        albums[albums.length - 1].setTitle(sc.nextLine());

        System.out.println("After: ");
        System.out.println(albums[albums.length - 1]);
    }
}
