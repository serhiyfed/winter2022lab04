public class Album {
    private double cost;
    private int pubYear;
    private String title;

    public Album(double cost, int pubYear, String title) {
        this.cost = cost;
        this.pubYear = pubYear;
        this.title = title;
    }

    public double getCost() {
        return cost;
    }

//    public void setCost(double cost) {
//        this.cost = cost;
//    }

    public int getPubYear() {
        return pubYear;
    }

    public void setPubYear(int pubYear) {
        this.pubYear = pubYear;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getCostAfterTax() {
        return cost * 1.15;
    }

    @Override
    public String toString() {
        return "Album{" +
                "cost=" + cost +
                ", pubYear=" + pubYear +
                ", title='" + title + '\'' +
                ", costAfterTax=" + getCostAfterTax() +
                '}';
    }
}
